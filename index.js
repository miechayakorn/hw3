const express = require('express')
const app = express()
const shop = require('./db')
const bodyParser = require('body-parser')

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

app.get('/', (req, res) => {
  res.sendFile(__dirname + '/index.html')
})

app.get('/shop', (req, res) => {
  res.json(shop)
})

app.get('/shop/:id', (req, res) => {
  res.json(shop.find(shop => shop.item === req.params.id))
})

app.post('/shops', (req, res) => {
  shop.push(req.body)
  res.status(201).json(shop)
})

app.post('/shopu/', (req, res) => {
  const updateIndex = shop.findIndex(shop => shop.item === req.body.item)
  Object.assign(shop[updateIndex], req.body)
  res.status(200).json(shop)
})

app.post('/shopd/', (req, res) => {
   //const deletedIndex = shop.findIndex(shop => shop.item === req.params.iteminput)
  const deletedIndex = shop.findIndex(shop => shop.item === req.body.item)
   shop.splice(deletedIndex, 1)
   res.status(200).json(shop)
})

app.listen(3000, () => {
  console.log('Start server at port 3000.')
})
